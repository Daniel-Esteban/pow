#!/bin/bash
handler()
{
    killall proof
    exit 0
}
trap handler SIGINT
wd=`pwd -P`
file="$wd/$1"
parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
ncores=`nproc --all`

min=0
max=$ncores
echo $#
if [[ $# -eq 3 ]]; then
    min=$2
    max=$3
fi
cnt=$min
maxl=$((min+ncores-1))
echo $max
while [ $cnt -le $maxl ]; do
    echo "BASH: Launching $cnt $max"
    "$parent_path"/proof $file $cnt $max &
    cnt=$((cnt+1))
done
sleep infinity
echo "TIME UP"
#killall proof
