#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <openssl/md5.h>
#include <openssl/sha.h>
#include <time.h>
#include <signal.h>

#define SUFFIX "G082022"


#define MD5 1
#define SHA1 2
#define SHA2 3

#ifndef HASH
#define HASH MD5
#endif

#ifndef LONG_SUFF
    typedef unsigned int myint;
    #define F_HEX "%08x"
#else
    typedef unsigned long long int myint;
    #define F_HEX "%016llx"
#endif

#if HASH==SHA1
    #define MY_HASH_LEN SHA_DIGEST_LENGTH
    typedef SHA_CTX t_ctx;
    #define Hash_Update(x,y,z) SHA1_Update(x,y,z)
    #define Hash_Init(x) SHA1_Init(x)
    #define Hash_Final(x,y) SHA1_Final(x,y)
    #define HASH_LEN 40


#elif HASH==SHA2
    #define MY_HASH_LEN SHA256_DIGEST_LENGTH
    typedef SHA256_CTX t_ctx;
    #define Hash_Update(x,y,z) SHA256_Update(x,y,z)
    #define Hash_Init(x) SHA256_Init(x)
    #define Hash_Final(x,y) SHA256_Final(x,y)
    #define HASH_LEN 64

#else
    #define MY_HASH_LEN MD5_DIGEST_LENGTH
    typedef MD5_CTX t_ctx;
    #define Hash_Update(x,y,z) MD5_Update(x,y,z)
    #define Hash_Init(x) MD5_Init(x)
    #define Hash_Final(x,y) MD5_Final(x,y)
    #define HASH_LEN 32
#endif

#define BUF_SIZE 500
#define FORMAT F_HEX " " SUFFIX
// '\n' +strlen(SUFFIX) + 8/16 bit hex + espacio + '\0'
#define EXTRA_LENGTH (strlen(FORMAT) + sizeof(myint)*2 +1)
#define MIN_CEROS 4


char frecover[FILENAME_MAX];
myint it;

void sig_handler(int signo)
{
    if (signo == SIGINT){
        FILE* f = fopen(frecover, "w");
        fprintf(f, F_HEX, it);
        fclose(f);
        exit(1);

    }
}
int file_exists(char* fname){
    return access( fname, F_OK ) != -1;
}

void sprint_hash(char* str, const unsigned char hash[MY_HASH_LEN]){
    int i = 0;
    for(i=0; i < MY_HASH_LEN; i++){
        sprintf(&str[2*i],"%02x", hash[i]);
    }
    str[2*i] = '\0';
}

int count_ceros(char* str){
    char ch = 0;
    int n = 0;
    do{
        ch = str[n];
        if(ch == '0')
            n++;
    }while(ch == '0');
    return n;
}

void print_hash(const unsigned char hash[MY_HASH_LEN]){
    int i = 0;
    for(i=0; i < MY_HASH_LEN; i++){
        printf("%02x", hash[i]);
    }
}

t_ctx hash_readfile(char *fname, unsigned char hash[MY_HASH_LEN]){
    int fd = open(fname, O_RDONLY);
    unsigned char buff[BUF_SIZE];
    int n = 0;
    t_ctx context;
    if(fd == -1){
        printf("ERROR FICERO\n");
        exit(1);
    }
    Hash_Init(&context);
    while ((n = read(fd, buff, BUF_SIZE))){
        Hash_Update(&context, buff, n);
    }
    return context;
}

void copy_hash(unsigned char copy[MY_HASH_LEN], unsigned char hash[MY_HASH_LEN]){
    memcpy(copy, hash, (MY_HASH_LEN * sizeof(unsigned char)));
}
void append_suffix(char* s_file, char* file, char* suffix){
    FILE *src, *dest;
    src = fopen(s_file, "r");
    dest = fopen(file, "w");
    char ch;
    if(src == NULL || dest == NULL){
        printf("ERROR\n");
        exit(1);
    }
    while( ( ch = fgetc(src) ) != EOF )
        fputc(ch, dest);
    fprintf(dest, "%s", suffix);
    fclose(src);
    fclose(dest);

}

void proof_of_work(t_ctx s_ctx, unsigned char s_hash[MY_HASH_LEN], myint min, myint max, char* s_file ,char file[FILENAME_MAX]){

    int ceros;
    int max0 = 0;
    t_ctx ctx;
    unsigned char hash[MY_HASH_LEN];
    char suff[EXTRA_LENGTH];
    char hash_hex[HASH_LEN +1];
    printf("["F_HEX"-"F_HEX"]\n", min, max);
    for (it = min; it < max; it++){
        ctx = s_ctx;
        copy_hash(hash, s_hash);
        sprintf(suff, FORMAT, it);
        Hash_Update(&ctx, suff, strlen(suff));
        Hash_Final(hash, &ctx);
        sprint_hash(hash_hex, hash);
        ceros = count_ceros(hash_hex);
        if(ceros > max0 && ceros >= MIN_CEROS){
            append_suffix(s_file, file, suff);
            printf("%d %s %s\n", ceros, suff, hash_hex);
            max0 = ceros;
        }
    }
}

int main(int argc, char* argv[]){
    signal(SIGINT, sig_handler);
    if(argc != 2 && argc != 4){
        printf("Error\n");
        exit(1);
    }
    myint min=0, max=-1;
    myint start=min, end=max;
    int num=1,nodes=1;
    if(argc == 4){
        num = atoi(argv[2]);
        nodes = atoi(argv[3]);
        start = (num)*(max/nodes);
        end = (num+1)*(max/nodes)-1;
        if(num == nodes-1){
            end=max;
        }
    }
    t_ctx ctx;
    unsigned char hash[MY_HASH_LEN];
    char file[FILENAME_MAX];
    ctx = hash_readfile(argv[1], hash);
    sprintf(file, "%s_%d.tmp", argv[1], num);
    sprintf(frecover, "%s_%d.rec_%d.tmp", argv[1], num, HASH);

    if(file_exists(frecover)){
        FILE* f = fopen(frecover, "r");
        fscanf(f,F_HEX, &start);
        fclose(f);
        printf("RECOVERED " F_HEX "\n", start);
    }

    proof_of_work(ctx, hash, start , end, argv[1], file);

    if(file_exists(frecover)){

    }
    printf("FIN PROOF %d\n", num);
    return 0;
}
